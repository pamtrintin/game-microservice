package com.ivacrod.game.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ivacrod.game.models.Jogo;
import com.ivacrod.game.models.Pergunta;
import com.ivacrod.game.repositories.JogoRepository;
import com.ivacrod.game.services.PerguntasService;
import com.ivacrod.game.services.RankingService;

@RestController
@CrossOrigin
@RequestMapping("/game")
public class JogoController {

	@Autowired
	JogoRepository jogoRepository;

	@Autowired
	PerguntasService perguntasService;

	@Autowired
	RankingService rankingService;

	@RequestMapping(method = RequestMethod.GET, path = "/start/{nomeUsuario}")
	public ResponseEntity<?> iniciarJogo(@PathVariable String nomeUsuario) {

		Jogo jogo = new Jogo();
		jogo.setNomeUsuario(nomeUsuario);
		jogo.setQtdePerguntas(3);
		jogo.setQtdeAcertos(0);

		//List<Pergunta> lista = perguntasService.getPerguntasHTTP(jogo.getQtdePerguntas());
		List<Pergunta> lista = perguntasService.getPerguntasMQ(jogo.getQtdePerguntas());
		
		jogo.setListaPerguntas(lista);

		if (jogo.getListaPerguntas().size() > 0) {
			jogoRepository.save(jogo);
			jogo.setListaPerguntas(null);
			return ResponseEntity.ok().body(jogo);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/start/perguntas/{idJogo}")
	public ResponseEntity<?> obterListaPerguntas(@PathVariable Long idJogo) {

		Optional<Jogo> jogoOriginal = jogoRepository.findById(idJogo);

		if (jogoOriginal != null) {
			return ResponseEntity.ok().body(jogoOriginal);
		}

		return ResponseEntity.notFound().build();
	}

	@RequestMapping(method = RequestMethod.POST, path = "/start/perguntas/{idJogo}")
	public ResponseEntity<?> atualizarJogo(@PathVariable Long idJogo, @RequestBody List<Pergunta> listaPerguntas) {

		Optional<Jogo> jogoOriginal = jogoRepository.findById(idJogo);
		List<Pergunta> listaAtualizada = new ArrayList<>();

		if (jogoOriginal != null && listaPerguntas != null) {
			for (Pergunta pergunta : listaPerguntas) {
				if (pergunta.getRespostaUsuario() == pergunta.getResposta()) {
					jogoOriginal.get().setQtdeAcertos(jogoOriginal.get().getQtdeAcertos() + 1);
				}

				if (pergunta.getRespostaUsuario() == 0) {
					listaAtualizada.add(pergunta);
				}
			}

			jogoOriginal.get().setListaPerguntas(listaAtualizada);
			jogoRepository.save(jogoOriginal.get());

			if (listaAtualizada.size() == 0) {
				// Atualizar Ranking porque é a última resposta do usuário
				// rankingService.setRankingHTTP("Perguntas1", jogoOriginal.get().getNomeUsuario(),jogoOriginal.get().getQtdeAcertos());

				new RankingService().setRankingMQ("Jogo1", jogoOriginal.get().getNomeUsuario(),jogoOriginal.get().getQtdeAcertos(), jogoOriginal.get().getQtdePerguntas());
			}

			return ResponseEntity.ok(null);
		}

		return ResponseEntity.notFound().build();
	}

}
