package com.ivacrod.game.repositories;

import org.springframework.data.repository.CrudRepository;

import com.ivacrod.game.models.Jogo;

public interface JogoRepository extends CrudRepository<Jogo, Long>{

}
